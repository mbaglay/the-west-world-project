package ua.mbaglay.messaging;

import ua.mbaglay.BaseGameEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by maxim on 06.08.17.
 */
public class EntityManager {
    private static EntityManager entityManager = new EntityManager();

    private Map<Integer, BaseGameEntity> entityMap = new HashMap<Integer, BaseGameEntity>();

    private EntityManager() {}

    public static EntityManager getInstance() {
        return entityManager;
    }

    public void registerEntity(BaseGameEntity newEntity) {
        entityMap.put(newEntity.getId(), newEntity);
    }

    public BaseGameEntity getEntityById(int id) {
        return entityMap.get(id);
    }

    public void removeEntity(BaseGameEntity entity) {
        entityMap.remove(entity.getId());
    }
}
