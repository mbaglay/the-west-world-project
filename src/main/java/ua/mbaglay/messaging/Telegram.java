package ua.mbaglay.messaging;

/**
 * Created by maxim on 06.08.17.
 */
public class Telegram implements Comparable {
    public final static double SmallestDelay = 0.25;

    public int sender;
    public int receiver;
    public MessageType message;
    public long dispatchTime;
    public String extraInfo;

    public Telegram() {
        this(-1, -1, null, -1, null);
    }

    public Telegram(int sender, int receiver, MessageType message, long dispatchTime) {
        this(sender, receiver, message, dispatchTime, null);
    }

    public Telegram(int sender, int receiver, MessageType message, long dispatchTime, String extraInfo) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
        this.dispatchTime = dispatchTime;
        this.extraInfo = extraInfo;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Telegram)) {
            return false;
        }
        Telegram t1 = this;
        Telegram t2 = (Telegram) obj;
        return (Math.abs(t1.dispatchTime - t2.dispatchTime) < SmallestDelay)
                && (t1.sender == t2.sender)
                && (t1.receiver == t2.receiver)
                && (t1.message == t2.message);
    }

    @Override
    public int hashCode() {
        int hash = this.sender;
        hash = hash + this.receiver;
        hash = hash + this.message.name().hashCode();
        return hash;
    }

    public int compareTo(Object o) {
        Telegram t1 = this;
        Telegram t2 = (Telegram) o;
        if (t1 == t2) {
            return 0;
        } else {
            return (t1.dispatchTime < t2.dispatchTime) ? -1 : 1;
        }
    }

    @Override
    public String toString() {
        return "time: " + dispatchTime + "  Sender: " + sender
                + "   Receiver: " + receiver + "   Message: " + message.name();
    }
}
