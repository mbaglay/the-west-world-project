package ua.mbaglay.messaging;

import ua.mbaglay.BaseGameEntity;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by maxim on 06.08.17.
 */
public class MessageDispatcher {

    private static MessageDispatcher instance = new MessageDispatcher();

    private Queue<Telegram> priorityQueue = new LinkedList<Telegram>();

    private void discharge(BaseGameEntity receiver, Telegram message) {
        if (!receiver.handleMessage(message)) {
            System.out.println("Message not handled " + message.toString());
        }
    }

    private MessageDispatcher() {}

    public static MessageDispatcher getInstance() {
        return instance;
    }

    public void dispatchMessage(int sender, int receiver, MessageType message, long delay, String extraInfo) {
        BaseGameEntity receiverEntity = EntityManager.getInstance().getEntityById(receiver);

        Telegram telegram = new Telegram(sender, receiver, message, 0, extraInfo);

        if (delay <= 0) {
            this.discharge(receiverEntity, telegram);
        } else {
            long currentTime = System.currentTimeMillis();

            telegram.dispatchTime = currentTime + delay;

            this.priorityQueue.add(telegram);
        }
    }

    public void dispatchDelayedMessages() {
        long currentTime = System.currentTimeMillis();

        while (!priorityQueue.isEmpty() && priorityQueue.peek().dispatchTime < currentTime
                && priorityQueue.peek().dispatchTime > 0) {
            Telegram telegram = priorityQueue.poll();

            BaseGameEntity entity = EntityManager.getInstance().getEntityById(telegram.receiver);


            discharge(entity, telegram);
        }
    }
}
