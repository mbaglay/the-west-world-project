package ua.mbaglay;

import ua.mbaglay.messaging.Telegram;

/**
 * Created by maxim on 05.08.17.
 */
public class StateMachine<T extends BaseGameEntity> {
    private T owner;
    private State<T> currentState;
    private State<T> previousState;
    private State<T> globalState;

    public StateMachine(T owner) {
        this.owner = owner;
        currentState = null;
        previousState = null;
        globalState = null;
    }

    public void setCurrentState(State<T> state) {
        currentState = state;
    }

    public void setPreviousState(State<T> state) {
        previousState = state;
    }

    public void setGlobalStateState(State<T> state) {
        globalState = state;
    }

    public void update() {
        if (globalState != null) {
            globalState.execute(owner);
        }

        if (currentState != null) {
            currentState.execute(owner);
        }
    }

    public void changeState(State<T> newState) {
        if (newState == null) {
            throw new RuntimeException("Trying to change to a null state");
        }

        this.previousState = this.currentState;
        this.currentState.exit(owner);
        this.currentState = newState;
        this.currentState.enter(owner);
    }

    public void revertToPreviousState() {
        this.changeState(this.previousState);
    }

    public State<T> getCurrentState() {
        return currentState;
    }

    public State<T> getPreviousState() {
        return previousState;
    }

    public State<T> getGlobalState() {
        return globalState;
    }

    public boolean isInState(State<T> state) {
        return this.currentState.getClass() == state.getClass();
    }

    public boolean handleMessage(Telegram telegram) {
        if (currentState != null && currentState.onMessage(owner, telegram)) {
            return true;
        }

        if (globalState != null && globalState.onMessage(owner, telegram)) {
            return true;
        }

        return false;
    }
}
