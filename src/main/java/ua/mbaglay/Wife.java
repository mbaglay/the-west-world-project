package ua.mbaglay;

import ua.mbaglay.messaging.Telegram;
import ua.mbaglay.states.wife.DoHouseWork;
import ua.mbaglay.states.wife.WifeGlobalState;

/**
 * Created by maxim on 06.08.17.
 */
public class Wife extends BaseGameEntity {

    private StateMachine<Wife> stateMachine;
    private Location location;
    private boolean cooking;

    public Wife(int id) {
        super(id);
        this.location = Location.SHACK;
        this.stateMachine = new StateMachine<Wife>(this);
        stateMachine.setCurrentState(DoHouseWork.getInstance());
        stateMachine.setGlobalStateState(WifeGlobalState.getInstance());
    }

    public void update() {
        stateMachine.update();
    }

    public StateMachine<Wife> getStateMachine() {
        return stateMachine;
    }

    public Location getLocation() {
        return location;
    }

    public void changeLocaltion(Location newLocation) {
        this.location = newLocation;
    }

    public boolean isCooking() {
        return cooking;
    }

    public void setCooking(boolean cooking) {
        this.cooking = cooking;
    }

    @Override
    public boolean handleMessage(Telegram telegram) {
        return this.stateMachine.handleMessage(telegram);
    }
}
