package ua.mbaglay.states.miner;

import ua.mbaglay.BaseGameEntity;
import ua.mbaglay.Miner;
import ua.mbaglay.State;
import ua.mbaglay.messaging.Telegram;

/**
 * Created by maxim on 07.08.17.
 */
public class EatStew extends State<Miner> {
    private static EatStew instance = new EatStew();

    private EatStew() {}

    public static EatStew getInstance() {
        return instance;
    }

    public void enter(Miner entity) {
        System.out.println(entity.getId() + ": Smells Reaaal goood Elsa!");
    }

    public void execute(Miner entity) {
        System.out.println(entity.getId() + ": Tastes real good too!");

        entity.getStateMachine().revertToPreviousState();
    }

    public void exit(Miner entity) {
        System.out.println(entity.getId()
                + ": Thankya li'lle lady. Ah better get back to whatever ah wuz doin'");
    }

    public boolean onMessage(BaseGameEntity entity, Telegram telegram) {
        return false;
    }
}
