package ua.mbaglay.states.miner;

import ua.mbaglay.BaseGameEntity;
import ua.mbaglay.Location;
import ua.mbaglay.Miner;
import ua.mbaglay.State;
import ua.mbaglay.messaging.Telegram;

/**
 * Created by maxim on 05.08.17.
 */
public class EnterMineAndDigForNugget extends State<Miner> {

    protected static State instance;
    private EnterMineAndDigForNugget() {}

    public static State getInstance() {
        if (instance == null) {
            instance = new EnterMineAndDigForNugget();
        }

        return instance;
    }

    @Override
    public void enter(Miner miner) {
        if (miner.getLocation() != Location.GOLD_MINE) {
            System.out.println(miner.getId() + ": Walkin' to the gold mine");

            miner.changeLocation(Location.GOLD_MINE);
        }
    }

    @Override
    public void execute(Miner miner) {
        miner.addGoldCarried(1);
        miner.increaseFatigue();

        System.out.println(miner.getId() + ": Pickin' up a nugget");

        if (miner.arePocketsFull()) {
            miner.getStateMachine().changeState(VisitBankAndDepositGold.getInstance());
        }

        if (miner.isThirsty()) {
            miner.getStateMachine().changeState(QuenchThirst.getInstance());
        }
    }

    @Override
    public void exit(Miner miner) {
        System.out.println(miner.getId() + ": Ah'm leavin' the gold mine with mah pockets full o' sweet gold");
    }

    @Override
    public boolean onMessage(BaseGameEntity entity, Telegram telegram) {
        return false;
    }
}
