package ua.mbaglay.states.miner;

import ua.mbaglay.BaseGameEntity;
import ua.mbaglay.Location;
import ua.mbaglay.Miner;
import ua.mbaglay.State;
import ua.mbaglay.messaging.Telegram;

/**
 * Created by maxim on 05.08.17.
 */
public class VisitBankAndDepositGold extends State<Miner> {

    protected static State instance;
    private VisitBankAndDepositGold() {}

    public static State getInstance() {
        if (instance == null) {
            instance = new VisitBankAndDepositGold();
        }

        return instance;
    }

    public void enter(Miner entity) {
        if (entity.getLocation() != Location.BANK) {
            System.out.println(entity.getId() + ": Goin' to the bank. Yes siree");

            entity.changeLocation(Location.BANK);
        }
    }

    public void execute(Miner entity) {
        entity.addToWealth(entity.getGoldCarried());
        entity.setGoldCarried(0);

        System.out.println(entity.getId() + ": Depositing gold. Total savings now: " + entity.getMoneyInBank());

        if (entity.getMoneyInBank() >= Miner.COMFORT_LEVEL) {
            System.out.println(entity.getId() + ": WooHoo! Rich enough for now. Back home to mah li'lle lady");

            entity.getStateMachine().changeState(GoHomeANdSleepTillRested.getInstance());
        } else {
            entity.getStateMachine().changeState(EnterMineAndDigForNugget.getInstance());
        }
    }

    public void exit(Miner entity) {
        System.out.println(entity.getId() + ": Leavin' the bank");
    }

    @Override
    public boolean onMessage(BaseGameEntity entity, Telegram telegram) {
        return false;
    }
}
