package ua.mbaglay.states.miner;

import ua.mbaglay.BaseGameEntity;
import ua.mbaglay.Location;
import ua.mbaglay.Miner;
import ua.mbaglay.State;
import ua.mbaglay.messaging.Telegram;

/**
 * Created by maxim on 05.08.17.
 */
public class QuenchThirst extends State<Miner> {

    protected static State instance;
    private QuenchThirst() {}

    public static State getInstance() {
        if (instance == null) {
            instance = new QuenchThirst();
        }

        return instance;
    }

    public void enter(Miner entity) {
        if (entity.getLocation() != Location.SALOON) {
            entity.changeLocation(Location.SALOON);

            System.out.println(entity.getId() + ": Boy, ah sure is thusty! Walking to the saloon");
        }
    }

    public void execute(Miner entity) {
        entity.buyAndDrinkAWhiskey();

        System.out.println(entity.getId() + ": That's mighty fine sippin liquer");

        entity.getStateMachine().changeState(EnterMineAndDigForNugget.getInstance());
    }

    public void exit(Miner entity) {
        System.out.println(entity.getId() + ": Leaving the saloon, feelin' good");
    }

    @Override
    public boolean onMessage(BaseGameEntity entity, Telegram telegram) {
        return false;
    }
}
