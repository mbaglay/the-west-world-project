package ua.mbaglay.states.miner;

import ua.mbaglay.BaseGameEntity;
import ua.mbaglay.Location;
import ua.mbaglay.Miner;
import ua.mbaglay.State;
import ua.mbaglay.messaging.MessageDispatcher;
import ua.mbaglay.messaging.MessageType;
import ua.mbaglay.messaging.Telegram;

/**
 * Created by maxim on 05.08.17.
 */
public class GoHomeANdSleepTillRested extends State<Miner> {

    protected static State instance;
    private GoHomeANdSleepTillRested() {}

    public static State getInstance() {
        if (instance == null) {
            instance = new GoHomeANdSleepTillRested();
        }

        return instance;
    }

    public void enter(Miner entity) {
        if (entity.getLocation() != Location.SHACK) {
            System.out.println(entity.getId() + ": Walkin' home");
            entity.changeLocation(Location.SHACK);

            MessageDispatcher.getInstance().dispatchMessage(entity.getId(), 1,
                    MessageType.HiHoneyImHome, 0, null);
        }
    }

    public void execute(Miner entity) {
        if (!entity.isFatigued()) {
            System.out.println(entity.getId() + ": What a God darn fantastic nap! Time to find more gold");
            entity.getStateMachine().changeState(EnterMineAndDigForNugget.getInstance());
        } else {
            entity.decreaseFatigue();
            System.out.println(entity.getId() + ": ZZZZZ...");
        }
    }

    public void exit(Miner entity) {
        System.out.println(entity.getId() + ": Leaving the house");
    }

    @Override
    public boolean onMessage(BaseGameEntity entity, Telegram telegram) {
        switch (telegram.message) {
            case StewReady: {
                System.out.println("\nMessage handled by " + entity.getId()
                        + " at time: " + System.currentTimeMillis());

                System.out.println("\n" + entity.getId()
                        + ": Okay hun, ahm a-comin'!");

                ((Miner) entity).getStateMachine().changeState(EatStew.getInstance());

                return true;
            }
        }

        return false;
    }
}
