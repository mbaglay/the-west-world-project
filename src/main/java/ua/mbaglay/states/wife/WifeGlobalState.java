package ua.mbaglay.states.wife;

import ua.mbaglay.BaseGameEntity;
import ua.mbaglay.State;
import ua.mbaglay.Wife;
import ua.mbaglay.messaging.Telegram;

import java.util.Random;

/**
 * Created by maxim on 06.08.17.
 */
public class WifeGlobalState extends State<Wife> {
    private static WifeGlobalState instance = new WifeGlobalState();

    private WifeGlobalState(){}

    public static WifeGlobalState getInstance() {
        return instance;
    }

    public void enter(Wife entity) {
    }

    public void execute(Wife entity) {
        if (new Random().nextFloat() <= 0.1) {
            entity.getStateMachine().changeState(VisitBathroom.getInstance());
        }
    }

    public void exit(Wife entity) {
    }

    public boolean onMessage(BaseGameEntity entity, Telegram telegram) {
        switch (telegram.message) {
            case HiHoneyImHome: {
                System.out.println("\nMessage handled by " + entity.getId()
                        + " at time: " + System.currentTimeMillis());

                System.out.println("\n" + entity.getId()
                        + ": Hi honey. Let me make you some of mah fine country stew");

                ((Wife) entity).getStateMachine().changeState(CookStew.getInstance());
            }
            return true;
        }

        return false;
    }
}
