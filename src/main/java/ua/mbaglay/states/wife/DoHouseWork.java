package ua.mbaglay.states.wife;

import ua.mbaglay.BaseGameEntity;
import ua.mbaglay.State;
import ua.mbaglay.Wife;
import ua.mbaglay.messaging.Telegram;

import java.util.Random;

/**
 * Created by maxim on 06.08.17.
 */
public class DoHouseWork extends State<Wife> {
    private static DoHouseWork instance = new DoHouseWork();

    private DoHouseWork() {}

    public static DoHouseWork getInstance() {
        return instance;
    }

    public void enter(Wife entity) {
        System.out.println(entity.getId() + ": Time to do some more housework!");
    }

    public void execute(Wife entity) {
        switch (new Random().nextInt() % 3) {
            case 0:
                System.out.println(entity.getId() + ": Moppin' the floor");
                break;
            case 1:
                System.out.println(entity.getId() + ": Washin' the dishes");
                break;
            case 2:
                System.out.println(entity.getId() + ": Makin' the bed");
                break;
        }
    }

    public void exit(Wife entity) {}

    @Override
    public boolean onMessage(BaseGameEntity entity, Telegram telegram) {
        return false;
    }
}
