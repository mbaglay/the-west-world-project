package ua.mbaglay.states.wife;

import ua.mbaglay.BaseGameEntity;
import ua.mbaglay.State;
import ua.mbaglay.Wife;
import ua.mbaglay.messaging.MessageDispatcher;
import ua.mbaglay.messaging.MessageType;
import ua.mbaglay.messaging.Telegram;

/**
 * Created by maxim on 06.08.17.
 */
public class CookStew extends State<Wife> {
    private static CookStew instance = new CookStew();

    private CookStew() {}

    public static CookStew getInstance() {
        return instance;
    }

    public void enter(Wife entity) {
        if (!entity.isCooking()) {
            System.out.println(entity.getId() + ": Putting the stew in the oven");

            MessageDispatcher.getInstance().dispatchMessage(entity.getId(), entity.getId(),
                    MessageType.StewReady, 5, null);

            entity.setCooking(true);
        }
    }

    public void execute(Wife entity) {
        System.out.println(entity.getId() +  ": Fussin' over food");
    }

    public void exit(Wife entity) {
        System.out.println(entity.getId() + ": Puttin' the stew on the table");
    }

    @Override
    public boolean onMessage(BaseGameEntity entity, Telegram telegram) {
        switch (telegram.message) {
            case StewReady: {
                System.out.println("\nMessage received by " + entity.getId()
                        + " at time: " + System.currentTimeMillis());

                System.out.println("\n" + entity.getId()
                        + ": Stew ready! Let's eat");

                MessageDispatcher.getInstance().dispatchMessage(entity.getId(), 0,
                        MessageType.StewReady, 0, null);

                ((Wife) entity).setCooking(false);

                ((Wife) entity).getStateMachine().changeState(DoHouseWork.getInstance());
            }
            return true;
        }

        return false;
    }
}
