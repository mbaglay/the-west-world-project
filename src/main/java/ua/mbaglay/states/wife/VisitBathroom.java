package ua.mbaglay.states.wife;

import ua.mbaglay.BaseGameEntity;
import ua.mbaglay.State;
import ua.mbaglay.Wife;
import ua.mbaglay.messaging.Telegram;

/**
 * Created by maxim on 06.08.17.
 */
public class VisitBathroom extends State<Wife> {
    private static VisitBathroom instance = new VisitBathroom();

    private VisitBathroom() {}

    public static VisitBathroom getInstance() {
        return instance;
    }

    public void enter(Wife entity) {
        System.out.println(entity.getId() + ": Walkin' to the can. Need to powda mah pretty li'lle nose");
    }

    public void execute(Wife entity) {
        System.out.println(entity.getId() + ": Ahhhhhh! Sweet relief!");
        entity.getStateMachine().revertToPreviousState();
    }

    public void exit(Wife entity) {
        System.out.println(entity.getId() + ": Leavin' the Jon");
    }

    @Override
    public boolean onMessage(BaseGameEntity entity, Telegram telegram) {
        return false;
    }
}
