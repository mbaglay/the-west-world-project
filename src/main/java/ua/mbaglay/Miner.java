package ua.mbaglay;

import ua.mbaglay.messaging.Telegram;
import ua.mbaglay.states.miner.GoHomeANdSleepTillRested;

/**
 * Created by maxim on 05.08.17.
 */
public class Miner extends BaseGameEntity {

    public static final int POCKET_SIZE = 5;
    public static final int THIRST_LIMIT = 8;
    public static final int COMFORT_LEVEL = 5;
    public static final int TIREDNESS_THRESHOLD = 5;

    private StateMachine<Miner> stateMachine;
    private Location location;
    private int goldCarried;
    private int moneyInBank;
    private int thirst;
    private int fatigue;

    public Miner(int id) {
        super(id);
        this.location = Location.SHACK;
        this.goldCarried = 0;
        this.moneyInBank = 0;
        this.thirst = 0;
        this.fatigue = 0;

        this.stateMachine = new StateMachine<Miner>(this);
        this.stateMachine.setCurrentState(GoHomeANdSleepTillRested.getInstance());
//        this.stateMachine.setGlobalStateState(MinerGlobalState.getInstance());
    }

    @Override
    public void update() {
        this.thirst++;

        this.stateMachine.update();
    }

    public StateMachine<Miner> getStateMachine() {
        return this.stateMachine;
    }

    public void changeLocation(Location location) {
        this.location = location;
    }

    public void addGoldCarried(int increase) {
        this.goldCarried += increase;
    }

    public boolean arePocketsFull() {
        if (this.goldCarried >= POCKET_SIZE) {
            return true;
        }

        return false;
    }

    public boolean isThirsty() {
        if (this.thirst >= THIRST_LIMIT) {
            return true;
        }

        return false;
    }

    public boolean isFatigued() {
        if (this.fatigue > TIREDNESS_THRESHOLD) {
            return true;
        }

        return false;
    }

    public void buyAndDrinkAWhiskey() {
        this.thirst = 0;
        this.moneyInBank -= 2;
    }

    public void increaseFatigue() {
        this.fatigue++;
    }

    public void decreaseFatigue() {
        this.fatigue--;
    }

    public void addToWealth(int gold) {
        this.moneyInBank += gold;
    }

    public Location getLocation() {
        return location;
    }

    public int getGoldCarried() {
        return goldCarried;
    }

    public void setGoldCarried(int goldCarried) {
        this.goldCarried = goldCarried;
    }

    public int getMoneyInBank() {
        return moneyInBank;
    }

    public int getThirst() {
        return thirst;
    }

    public int getFatigue() {
        return fatigue;
    }

    @Override
    public boolean handleMessage(Telegram telegram) {
        return stateMachine.handleMessage(telegram);
    }
}
