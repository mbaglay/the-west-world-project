package ua.mbaglay;

import ua.mbaglay.messaging.Telegram;

/**
 * Created by maxim on 05.08.17.
 */
public abstract class BaseGameEntity {

    private int id;

    private static int nextValidId = 0;

    public BaseGameEntity() {
        setId(0);
    }

    public BaseGameEntity(int id) {
        setId(id);
    }

    public int getId() {
        return this.id;
    }

    private void setId(int value) {
        if (value >= nextValidId) {
            this.id = value;
            nextValidId = value++;
        } else {
            this.id = nextValidId++;
        }
    }

    public abstract void update();

    public abstract boolean handleMessage(Telegram telegram);
}
