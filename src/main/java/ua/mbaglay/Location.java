package ua.mbaglay;

/**
 * Created by maxim on 05.08.17.
 */
public enum Location {
    SALOON, BANK, SHACK, GOLD_MINE
}
