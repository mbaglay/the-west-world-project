package ua.mbaglay;

import ua.mbaglay.messaging.EntityManager;

/**
 * Created by maxim on 05.08.17.
 */
public class Main {
    private static final int STEPS_TO_RUN = 50;

    public static void main(String[] args) {
        Miner miner = new Miner(0);
        EntityManager.getInstance().registerEntity(miner);
        Wife wife = new Wife(1);
        EntityManager.getInstance().registerEntity(wife);

        for (int i = 0; i < STEPS_TO_RUN; i++) {
            miner.update();
            wife.update();
        }
    }
}
