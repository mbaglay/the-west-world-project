package ua.mbaglay;

import ua.mbaglay.messaging.Telegram;

/**
 * Created by maxim on 05.08.17.
 */
public abstract class State<T> {
    public abstract void enter(T entity);
    public abstract void execute(T entity);
    public abstract void exit(T entity);
    public abstract boolean onMessage(BaseGameEntity entity, Telegram telegram);
}
